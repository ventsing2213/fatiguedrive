package com.baidu.fatiguedrive;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.MessageDigest;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

import android.util.Log;

public class PushTrigger {
	private static final String LOG_TAG = "PushTrigger";
	private static final String HTTP_METHOD = "POSThttp://channel.api.duapp.com/rest/2.0/channel/channel";

	private final String api_key;
	private final String secret_key;
	private long timestamp = 0;

	public PushTrigger(String ak, String sk) {
		api_key = ak;
		secret_key = sk;
	}

	public PushTrigger() {
		api_key = "GCr6Kze99w7LjSRY5Bmxe2iM";
		secret_key = "mR9xRAmSP7bAADeA05VC8ccjLfgbQHSy";
	}

	public void push() {
		String sign = generateSign();

		if (sign == null)
			return;

		StringBuilder builder = new StringBuilder();
		final String CHANNEL_URL = "http://channel.api.duapp.com/rest/2.0/channel/channel?method=push_msg";
		builder.append(CHANNEL_URL);

		final String API_KEY = "&apikey=" + api_key;
		builder.append(API_KEY);

		builder.append("&sign=" + sign);

		final String TIMESTAMP = "&timestamp=" + timestamp;
		builder.append(TIMESTAMP);

		// push type: push to all
		builder.append("&push_type=3");

		// msg
		builder.append("&messages=test&msg_keys=testkey");

		final String URLSTRING = builder.toString();

		Log.d(LOG_TAG, "request URL:" + URLSTRING);

		HttpPost httpRequest = new HttpPost(URLSTRING);
		try {
			HttpResponse httpResponse = new DefaultHttpClient()
					.execute(httpRequest);
			if (httpResponse.getStatusLine().getStatusCode() == 200) {
				Log.d(LOG_TAG, "send push msg success.");
			} else {
				Log.d(LOG_TAG, "send push msg failed.");
			}

		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private String generateSign() {
		StringBuilder builder = new StringBuilder();
		builder.append(HTTP_METHOD);

		final String API_KEY = "apikey=" + api_key;
		builder.append(API_KEY);

		final String MESSAGE = "messages=test";
		builder.append(MESSAGE);

		final String METHOD = "method=push_msg";
		builder.append(METHOD);

		final String MESSAGE_KEY = "msg_keys=testkey";
		builder.append(MESSAGE_KEY);

		final String PUSH_TYPE = "push_type=3";
		builder.append(PUSH_TYPE);

		timestamp = System.currentTimeMillis();
		final String TIMESTAMP = "timestamp=" + timestamp;
		builder.append(TIMESTAMP);

		builder.append(secret_key);

		String sign = null;
		try {
			sign = MD5(URLEncoder.encode(builder.toString(), "utf-8")
					.getBytes());
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return sign;
	}

	public static String MD5(byte[] btInput) {
		char hexDigits[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
				'a', 'b', 'c', 'd', 'e', 'f' };
		try {

			// 获得MD5摘要算法的 MessageDigest 对象
			MessageDigest mdInst = MessageDigest.getInstance("MD5");
			// 使用指定的字节更新摘要
			mdInst.update(btInput);
			// 获得密文
			byte[] md = mdInst.digest();
			// 把密文转换成十六进制的字符串形式
			int j = md.length;
			char str[] = new char[j * 2];
			int k = 0;
			for (int i = 0; i < j; i++) {
				byte byte0 = md[i];
				str[k++] = hexDigits[byte0 >>> 4 & 0xf];
				str[k++] = hexDigits[byte0 & 0xf];
			}
			return new String(str);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

}
