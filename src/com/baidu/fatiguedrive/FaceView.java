package com.baidu.fatiguedrive;

import static com.googlecode.javacv.cpp.opencv_core.IPL_DEPTH_8U;
import static com.googlecode.javacv.cpp.opencv_core.cvClearMemStorage;
import static com.googlecode.javacv.cpp.opencv_core.cvGetSeqElem;
import static com.googlecode.javacv.cpp.opencv_core.cvLoad;
import static com.googlecode.javacv.cpp.opencv_core.cvFlip;
import static com.googlecode.javacv.cpp.opencv_core.cvSetImageROI;
import static com.googlecode.javacv.cpp.opencv_core.cvResetImageROI;
import static com.googlecode.javacv.cpp.opencv_objdetect.CV_HAAR_DO_CANNY_PRUNING;
import static com.googlecode.javacv.cpp.opencv_objdetect.CV_HAAR_FIND_BIGGEST_OBJECT;
import static com.googlecode.javacv.cpp.opencv_objdetect.CV_HAAR_SCALE_IMAGE;
import static com.googlecode.javacv.cpp.opencv_objdetect.cvHaarDetectObjects;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;

import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.hardware.Camera;
import android.util.Log;
import android.view.View;

import com.googlecode.javacpp.Loader;

import com.googlecode.javacv.cpp.opencv_flann;
import com.googlecode.javacv.*;
import com.googlecode.javacv.cpp.ARToolKitPlus.Logger;
import com.googlecode.javacv.cpp.opencv_core.CvMemStorage;
import com.googlecode.javacv.cpp.opencv_core.CvRect;
import com.googlecode.javacv.cpp.opencv_core.CvSeq;
import com.googlecode.javacv.cpp.opencv_core.IplImage;
import com.googlecode.javacv.cpp.opencv_objdetect;
import com.googlecode.javacv.cpp.opencv_objdetect.CvHaarClassifierCascade;


class FaceView extends View implements Camera.PreviewCallback {
	public static final int SUBSAMPLING_FACTOR = 2;
	public static final String ACTION_FATIGUEDRIVE = "com.baidu.action.fatiguedrive";

	private Context mContext;
	private Paint mPaint;
	
	private IplImage grayImage;
	private IplImage copyGrayImage;
	private CvHaarClassifierCascade classifier;
	private CvHaarClassifierCascade eyeClassifier;
	private CvMemStorage storage;
	private CvMemStorage eyeStorage;
	private CvSeq faces;
	private CvSeq eyes;
	private int initFaceFrm = 0;
	private int initEyeCount = 0;
	private int faceFrm = 0;
	private int eyeCount = 0;
	private boolean initDetected =false;
	private boolean fatiguedrive = false;

	public FaceView(Context context) throws IOException {
		super(context);

		mContext = context;
		loadClassifierFromJavaRes(context, 
				"/com/baidu/fatiguedrive/haarcascade_frontalface_alt2.xml");
		loadClassifierFromJavaRes(context, 
				"/com/baidu/fatiguedrive/haarcascade_eye_tree_eyeglasses.xml");
		storage = CvMemStorage.create();
		eyeStorage = CvMemStorage.create();
		
		mPaint = new Paint();
		mPaint.setColor(Color.WHITE);
		mPaint.setTextSize(20);
	}

	public void loadClassifierFromJavaRes(Context context,String filePath) throws IOException{
		File classifierFile = Loader
				.extractResource(
						getClass(),
						filePath,
						context.getCacheDir(), "classifier", ".xml");
		if (classifierFile == null || classifierFile.length() <= 0) {
			throw new IOException(
					"Could not extract the classifier file from Java resource.");
		}

		// Preload the opencv_objdetect module to work around a known bug.
		Loader.load(opencv_objdetect.class);
		CvHaarClassifierCascade cascade =null;
		cascade = new CvHaarClassifierCascade(
				cvLoad(classifierFile.getAbsolutePath()));
		if(filePath.equals("/com/baidu/fatiguedrive/haarcascade_frontalface_alt2.xml")){
			classifier = cascade;
		}else{
			eyeClassifier = cascade;
		}
		classifierFile.delete();
		if (cascade.isNull()) {
			throw new IOException("Could not load the classifier file.");
		}
	}
	
	public void onPreviewFrame(final byte[] data, final Camera camera) {
		try {
			Camera.Size size = camera.getParameters().getPreviewSize();
			processImage(data, size.width, size.height);
			camera.addCallbackBuffer(data);
		} catch (RuntimeException e) {
			// The camera has probably just been released, ignore.
		}
	}

	protected void processImage(byte[] data, int width, int height) {
		// First, downsample our image and convert it into a grayscale IplImage
		int f = SUBSAMPLING_FACTOR;
		if (grayImage == null || grayImage.width() != width / f
				|| grayImage.height() != height / f) {
			grayImage = IplImage.create(width / f, height / f, IPL_DEPTH_8U, 1);
		}
		
		/*if (copyGrayImage == null
				|| copyGrayImage.width() != grayImage.width() / f
				|| copyGrayImage.height() != grayImage.height() / f) {
			copyGrayImage = IplImage.create(width / f, height / f,
					IPL_DEPTH_8U, 1);
		}*/

		int imageWidth = grayImage.width();
		int imageHeight = grayImage.height();
		int dataStride = f * width;
		int imageStride = grayImage.widthStep();
		ByteBuffer imageBuffer = grayImage.getByteBuffer();
		for (int y = 0; y < imageHeight; y++) {
			int dataLine = y * dataStride;
			int imageLine = y * imageStride;
			for (int x = 0; x < imageWidth; x++) {
				imageBuffer.put(imageLine + imageWidth - 1 - x, data[dataLine
						+ f * x]);
			}
		}
		// Not work ,I don't know why
		// cvFlip(grayImage,copyGrayImage,0);
		cvClearMemStorage(storage);
		faces = cvHaarDetectObjects(grayImage, classifier, storage, 1.1, 3,
				CV_HAAR_FIND_BIGGEST_OBJECT | CV_HAAR_DO_CANNY_PRUNING
						| CV_HAAR_SCALE_IMAGE);


		
		if (faces!=null && faces.total() != 0) {
			if(initDetected){
				++faceFrm;
			}else{
				++initFaceFrm;
				if(initFaceFrm>6){
					if(((float)(initEyeCount)/initFaceFrm)>0.5){
						initDetected = true;
					}
					initFaceFrm = 0;
					initEyeCount = 0;
				}
			}
			CvRect r = new CvRect(cvGetSeqElem(faces, 0));
			cvSetImageROI(grayImage,r);
			cvClearMemStorage(eyeStorage);
			eyes = cvHaarDetectObjects(grayImage, eyeClassifier, eyeStorage,
					1.05, 2, CV_HAAR_DO_CANNY_PRUNING | CV_HAAR_SCALE_IMAGE);
			cvResetImageROI(grayImage);
			if(eyes!=null && eyes.total()!=0){
				if(initDetected){
					eyeCount+=2;
				}else{
					initEyeCount+=2;
				}
			}
			if ((initDetected)&&faceFrm%20 == 0)
			{
				if (((eyeCount>>1) <faceFrm*0.75))
				{
					Log.d(VIEW_LOG_TAG, "faceFrm is "+faceFrm + " eyeCount is "+eyeCount);				
					Intent intent = new Intent(ACTION_FATIGUEDRIVE);
					mContext.sendBroadcast(intent);
					initDetected = false;
					faceFrm =0;
					eyeCount = 0;
				}
				faceFrm =0;
				eyeCount = 0;
			}
		}
		postInvalidate();
	}

	@Override
	protected void onDraw(Canvas canvas) {
		String s = "FacePreview - This side up.";
		float textWidth = mPaint.measureText(s);
		canvas.drawText(s, (getWidth() - textWidth) / 2, 20, mPaint);

		if (faces !=null) {
			mPaint.setStrokeWidth(2);
			mPaint.setStyle(Paint.Style.STROKE);
			float scaleX = (float) getWidth() / grayImage.width();
			float scaleY = (float) getHeight() / grayImage.height();
			int total = faces.total();

			for (int i = 0; i < total; i++) {
				CvRect r = new CvRect(cvGetSeqElem(faces, i));
				int x = r.x(), y = r.y(), w = r.width(), h = r.height();
				canvas.drawRect(x * scaleX, y * scaleY, (x + w) * scaleX,
						(y + h) * scaleY, mPaint);
				if (eyes != null) {
					int eyeTotal = eyes.total();
					for (int j = 0; j < eyeTotal; ++j) {
						CvRect eyer = new CvRect(cvGetSeqElem(eyes, j));
						int eyex = r.x()+eyer.x(), eyey = r.y()+eyer.y(), eyew = eyer
								.width(), eyeh = eyer.height();
						canvas.drawRect(eyex * scaleX, eyey * scaleY,
								(eyex + eyew) * scaleX, (eyey + eyeh) * scaleY,
								mPaint);
					}
				}
			}

		} else {
			Log.d(VIEW_LOG_TAG, "didnt found any faces.");
		}
	}
}
