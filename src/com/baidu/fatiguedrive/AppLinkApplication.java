package com.baidu.fatiguedrive;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

import android.app.Application;
import android.os.Environment;
import android.os.Process;
import android.os.Vibrator;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.BDNotifyListener;
import com.baidu.location.LocationClient;
import com.baidu.mapapi.BMapManager;
import com.baidu.mapapi.MKEvent;
import com.baidu.mapapi.MKGeneralListener;
import com.baidu.mapapi.OverlayItem;

public class AppLinkApplication extends Application {

	public LocationClient mLocationClient = null;
	static public List<OverlayItem> mLocationGeoList = new ArrayList<OverlayItem>();
	private String mData;
	public MyLocationListenner myListener = new MyLocationListenner();
	public TextView mTv;
	public NotifyLister mNotifyer = null;
	public Vibrator mVibrator01;
	static AppLinkApplication mDemoApp;
	public static String mAddressName = "";

	// 百度MapAPI的管理类
	BMapManager mBMapMan = null;

	// 授权Key
	// 申请地址：http://developer.baidu.com/map/android-mobile-apply-key.htm
	String mStrKey = "10F6751E2AAE298BC1FD0ECE1130F9DDF7310BC2";
	boolean m_bKeyRight = true; // 授权Key正确，验证通过

	// 常用事件监听，用来处理通常的网络错误，授权验证错误等
	static class MyGeneralListener implements MKGeneralListener {
		@Override
		public void onGetNetworkState(int iError) {
			Log.d("MyGeneralListener", "onGetNetworkState error is " + iError);
			Toast.makeText(AppLinkApplication.mDemoApp.getApplicationContext(),
					"您的网络出错啦！", Toast.LENGTH_LONG).show();
		}

		@Override
		public void onGetPermissionState(int iError) {
			Log.d("MyGeneralListener", "onGetPermissionState error is "
					+ iError);
			if (iError == MKEvent.ERROR_PERMISSION_DENIED) {
				// 授权Key错误：
				Toast.makeText(
						AppLinkApplication.mDemoApp.getApplicationContext(),
						"请在BMapApiDemoApp.java文件输入正确的授权Key！", Toast.LENGTH_LONG)
						.show();
				AppLinkApplication.mDemoApp.m_bKeyRight = false;
			}
		}
	}

	@Override
	public void onCreate() {
		mLocationClient = new LocationClient(getApplicationContext());
		mLocationClient.registerLocationListener(myListener);

		Log.v("BMapApiDemoApp", "onCreate");
		mDemoApp = this;
		mBMapMan = new BMapManager(this);
		boolean isSuccess = mBMapMan
				.init(this.mStrKey, new MyGeneralListener());
		// 初始化地图sdk成功，设置定位监听时间
		if (isSuccess) {
			mBMapMan.getLocationManager().setNotifyInternal(10, 5);
		} else {
			// 地图sdk初始化失败，不能使用sdk
		}

		super.onCreate();
		Log.d("locSDK_Demo1",
				"... Application onCreate... pid=" + Process.myPid());
	}

	@Override
	// 建议在您app的退出之前调用mapadpi的destroy()函数，避免重复初始化带来的时间消耗
	public void onTerminate() {
		if (mBMapMan != null) {
			mBMapMan.destroy();
			mBMapMan = null;
		}
		super.onTerminate();
	}

	/**
	 * 显示字符串
	 * 
	 * @param str
	 */
	public void logMsg(String str) {
		try {
			mData = str;
			if (mTv != null)
				mTv.setText(mData);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 监听函数，又新位置的时候，格式化成字符串，输出到屏幕中
	 */
	public class MyLocationListenner implements BDLocationListener {
		@Override
		public void onReceiveLocation(BDLocation location) {
			
			if (location == null)
				return;
			StringBuffer sb = new StringBuffer(256);
			sb.append("time : ");
			sb.append(location.getTime());
			sb.append("\nerror code : ");
			sb.append(location.getLocType());
			sb.append("\nlatitude : ");
			sb.append(location.getLatitude());
			sb.append("\nlontitude : ");
			sb.append(location.getLongitude());
			sb.append("\nradius : ");
			sb.append(location.getRadius());
			if (location.getLocType() == BDLocation.TypeGpsLocation) {
				sb.append("\nspeed : ");
				sb.append(location.getSpeed());
				sb.append("\nsatellite : ");
				sb.append(location.getSatelliteNumber());
			} else if (location.getLocType() == BDLocation.TypeNetWorkLocation) {
				sb.append("\naddr : ");
				sb.append(location.getAddrStr());
				mAddressName = location.getAddrStr();
			}
			
			
			sb.append("\nsdk version : ");
			sb.append(mLocationClient.getVersion());
			Log.e("222", "location sus the time:" + System.currentTimeMillis()
					+ " " + sb.toString());
			String content = null;
			StringBuffer sb1 = new StringBuffer(256);
			sb1.append("\nlatitude : ");
			sb1.append(location.getLatitude());
			sb1.append("\nlontitude : ");
			sb1.append(location.getLongitude());

			if (location.getLocType() == BDLocation.TypeNetWorkLocation) {
				sb1.append("\naddr : ");
				sb1.append(location.getAddrStr());

			}
			content = sb1.toString();
			try {
				saveToSDCard("location1.txt", content);
			} catch (Exception e) {
				e.printStackTrace();
			}
			logMsg(sb.toString());
		}

		public void saveToSDCard(String filename, String content)
				throws Exception {
			// 通过getExternalStorageDirectory方法获取SDCard的文件路径
			File file = new File(Environment.getExternalStorageDirectory(),
					filename);
			// 获取输出流
			FileOutputStream outStream = new FileOutputStream(file, true);
			outStream.write(content.getBytes());
			outStream.close();
		}

		public void onReceivePoi(BDLocation poiLocation) {
			if (poiLocation == null) {
				return;
			}
			StringBuffer sb = new StringBuffer(256);
			sb.append("Poi time : ");
			sb.append(poiLocation.getTime());
			sb.append("\nerror code : ");
			sb.append(poiLocation.getLocType());
			sb.append("\nlatitude : ");
			sb.append(poiLocation.getLatitude());
			sb.append("\nlontitude : ");
			sb.append(poiLocation.getLongitude());
			sb.append("\nradius : ");
			sb.append(poiLocation.getRadius());
			if (poiLocation.getLocType() == BDLocation.TypeNetWorkLocation) {
				sb.append("\naddr : ");
				sb.append(poiLocation.getAddrStr());
			}
			if (poiLocation.hasPoi()) {
				sb.append("\nPoi:");
				sb.append(poiLocation.getPoi());
			} else {
				sb.append("noPoi information");
			}
			logMsg(sb.toString());
		}
	}

	/**
	 * 位置提醒回调函数
	 */
	public class NotifyLister extends BDNotifyListener {
		public void onNotify(BDLocation mlocation, float distance) {
			mVibrator01.vibrate(1000);
		}
	}
}
