/**Ford Motor Company
 * September 2012
 * Elizabeth Halash
 */

package com.baidu.fatiguedrive;

import java.io.File;
import java.io.IOException;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.MediaPlayer;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.telephony.SmsManager;
import android.util.Log;

import com.baidu.android.pushservice.PushConstants;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.iflytek.speech.SpeechError;
import com.iflytek.speech.SynthesizerPlayer;
import com.iflytek.speech.SynthesizerPlayerListener;

public class AppLinkService extends Service {

	String TAG = "hello";
	// variable used to increment correlation ID for every request sent to SYNC
	public int autoIncCorrId = 0;
	// variable to contain the current state of the service
	private static AppLinkService instance = null;
	// variable to contain the current state of the main UI ACtivity
	private MainActivity currentUIActivity;

	public boolean isLocationStart = false;
	public boolean isRegBtn = false;
	public boolean isAudioStatus = false;
	public LocationClient mLocClient = null;

	public int choiceSetID;
	public static String EYE_EDTECTED_ACTION = "com.baidu.action.fatiguedrive";
	public static String SEND_SMS_ACTION = "com.baidu.action.sendsms";
	public static String PAUSE_MUSIC_ACTION = "com.applink.test.pause";
	public static String CREATE_ALERT_ACTION = "com.applink.test.alert";

	static String EMERGENTNUMBER = "18210977127";

	private MediaPlayer mp;
	private PushTrigger pt;

	static final int MSG_VOICE_ALERT = 1000;
	static final int MSG_PUSH_COMMAND = 2000;
	static final int MSG_SEND_SMS = 3000;

	private Looper mServiceLooper;
	private ServiceHandler mServiceHandler;

	public void play(String path) {
		if (mp == null)
			mp = new MediaPlayer();
		try {
			mp.setDataSource(Environment.getExternalStorageDirectory()
					+ File.separator + path);
			mp.prepare();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		mp.start();
	}

	private void stop() {
		if (mp != null && mp.isPlaying()) {
			mp.stop();
			mp.reset();
		}
	}

	public static AppLinkService getInstance() {
		return instance;
	}

	public MainActivity getCurrentActivity() {
		return currentUIActivity;
	}

	public void setCurrentActivity(MainActivity currentActivity) {
		this.currentUIActivity = currentActivity;
	}

	public void onCreate() {
		super.onCreate();
		instance = this;
		registerDetectedEventListener();
		HandlerThread thread = new HandlerThread(TAG,
				Process.THREAD_PRIORITY_BACKGROUND);
		thread.start();

		mServiceLooper = thread.getLooper();
		mServiceHandler = new ServiceHandler(mServiceLooper);

		startLocation();
	}

	public void createAlert() {
		Log.d(TAG, "start voice alert.");
		try {
			String text = AppLinkApplication.mAddressName;
			if (text != null && text.length() > 0) {
				text += "附近有车主疲劳驾驶请注意安全绕行该地段 ";
				speakMessageIflytekforAlert(text);
			} else {
				Log.d(TAG, "can not get address.");
			}

		} catch (Exception e) {
			Log.e("111", "create alert error:" + e.toString());
		}
	}

	private BroadcastReceiver mDetectedEventReceiver = null;

	public void registerDetectedEventListener() {

		if (mDetectedEventReceiver == null) {
			mDetectedEventReceiver = new BroadcastReceiver() {
				@Override
				public void onReceive(Context context, Intent intent) {
					String action = intent.getAction();
					Log.e("111", "detectedevent action:" + action);
					if (action != null && action.equals(PAUSE_MUSIC_ACTION)) {
						stop();
					}
					if (action != null && action.equals(CREATE_ALERT_ACTION)) {
						Log.e("111", "the alert action");
						createAlert();
					}
				}
			};
			IntentFilter iFilter = new IntentFilter();
			// iFilter.addAction(EYE_EDTECTED_ACTION);
			iFilter.addAction(CREATE_ALERT_ACTION);
			iFilter.addAction(PAUSE_MUSIC_ACTION);
			registerReceiver(mDetectedEventReceiver, iFilter);
		}

	}

	/**
	 * �ͷŹ㲥
	 * 
	 * @param receiver
	 */
	private void releaseReceiver(BroadcastReceiver receiver) {
		if (receiver != null) {
			unregisterReceiver(receiver);
			receiver = null;
		}
	}

	private void startLocation() {
		if (mLocClient == null)
			mLocClient = ((AppLinkApplication) getApplication()).mLocationClient;
		setLocationOption();

		if (!isLocationStart) {
			mLocClient.start();
			isLocationStart = true;
		}

		if (mLocClient != null && mLocClient.isStarted()) {
			mLocClient.requestLocation();
		} else {
			Log.d("LocSDK3", "locClient is null or not started");
		}

	}

	private void setLocationOption() {
		LocationClientOption option = new LocationClientOption();
		option.setOpenGps(true); 
		option.setAddrType("all"); 
		option.setCoorType("bd09ll");
		option.setScanSpan(5000);
		option.disableCache(true);
		mLocClient.setLocOption(option);
	}

	public SynthesizerPlayerListener synbgListener = new SynthesizerPlayerListener() {
		@Override
		public void onPlayBegin() {

		}

		@Override
		public void onBufferPercent(int percent, int beginPos, int endPos) {

		}

		@Override
		public void onPlayPaused() {

		}

		@Override
		public void onPlayResumed() {

		}

		@Override
		public void onPlayPercent(int percent, int beginPos, int endPos) {

		}

		@Override
		public void onEnd(SpeechError arg0) {

		}

	};
	public SynthesizerPlayerListener synbgListener1 = new SynthesizerPlayerListener() {
		@Override
		public void onPlayBegin() {

		}

		@Override
		public void onBufferPercent(int percent, int beginPos, int endPos) {

		}

		@Override
		public void onPlayPaused() {

		}

		@Override
		public void onPlayResumed() {

		}

		@Override
		public void onPlayPercent(int percent, int beginPos, int endPos) {

		}

		@Override
		public void onEnd(SpeechError arg0) {
			// TODO Auto-generated method stub
			mServiceHandler.sendEmptyMessage(MSG_SEND_SMS);

			play("123.mp3");
		}

	};

	// ����Ѷ�ɵ�������
	public void speakMessageIflytek(String text) {
		SynthesizerPlayer player = SynthesizerPlayer.createSynthesizerPlayer(
				this, "appid=5099d80f");
		// player.setVoiceName("xiaoyu");
		player.setVolume(100);
		player.playText(text, null, synbgListener1);
	}

	public void speakMessageIflytekforAlert(String text) {
		SynthesizerPlayer player = SynthesizerPlayer.createSynthesizerPlayer(
				this, "appid=5099d80f");
		// player.setVoiceName("xiaoyu");
		player.setVolume(100);
		player.playText(text, null, synbgListener);
	}

	public int onStartCommand(Intent intent, int flags, int startId) {

		if (intent != null) {

			final String action = intent.getAction();
			if (action != null && action.equals(PushConstants.ACTION_MESSAGE)) {
				Log.d(TAG, "receive push msg.");

				mServiceHandler.sendEmptyMessage(MSG_PUSH_COMMAND);
			} else if (action != null && action.equals(EYE_EDTECTED_ACTION)) {

				mServiceHandler.sendEmptyMessage(MSG_VOICE_ALERT);

			} else if (action != null && action.equals(SEND_SMS_ACTION)) {
				mServiceHandler.sendEmptyMessage(MSG_SEND_SMS);

			} 
		}
		if (MainActivity.getInstance() != null) {
			setCurrentActivity(MainActivity.getInstance());
		}

		return START_STICKY;
	}

	public void sendMsg(String phoneNumber, String message) {
		// emergency call no.

		SmsManager smsManager = SmsManager.getDefault();
		smsManager.sendTextMessage(phoneNumber, null, message, null, null);

	}

	private final class ServiceHandler extends Handler {
		public ServiceHandler(Looper looper) {
			super(looper);
		}

		@Override
		public void handleMessage(Message msg) {
			// TODO Auto-generated method stub
			switch (msg.what) {
			case MSG_PUSH_COMMAND:
				Log.d(TAG, "get push message.");
				createAlert();
				break;
			case MSG_VOICE_ALERT:
				speakMessageIflytek("请注意 系统检测到您正在疲劳驾驶 请您注意行车安全");
				break;
			case MSG_SEND_SMS:
				Log.d(TAG, "send emergency sms.");
				final String message = "您好，百百度先生现在在疲劳驾驶。请关注他的行车安全。车辆目前行驶路段为"
						+ AppLinkApplication.mAddressName + "附近。";
				if (EMERGENTNUMBER != null) {
					sendMsg(EMERGENTNUMBER, message);
				}
				if (pt == null) {
					pt = new PushTrigger();
				}
				if (MainActivity.pushStatus) {
					try {
						Thread.sleep(10000); // wait 10 seconds and send push request.
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					pt.push();
				}
				break;
			default:
				super.handleMessage(msg);
			}
		}
	}

	public void onDestroy() {
		instance = null;
		releaseReceiver(mDetectedEventReceiver);
		super.onDestroy();
	}

	public void onError(String info, Exception e) {
		// TODO Auto-generated method stub
	}

	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}
}
