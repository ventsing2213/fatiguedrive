/**Ford Motor Company
 * September 2012
 * Elizabeth Halash
 */

package com.baidu.fatiguedrive;

import java.io.IOException;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.hardware.Camera;
import android.os.Bundle;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;

import com.baidu.android.pushservice.PushConstants;
import com.baidu.android.pushservice.PushManager;
import com.baidu.location.LocationClient;

public class MainActivity extends Activity {

	private static final String TAG = "hello";
	private static MainActivity instance = null;
	private boolean activityOnTop;
	public LocationClient mLocClient;
	public boolean isLocationStart = false;
	public int choiceSetID;

	private CameraPreview mPreview;
	private FaceView faceView;
	private Button mPushBtn;
	private Button mStopBtn;

	private WakeLock mWakeLock;

	static boolean pushStatus = false;

	public static MainActivity getInstance() {
		return instance;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_main);

		final PowerManager powermanager = (PowerManager) getSystemService(Context.POWER_SERVICE);
		mWakeLock = powermanager.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK
				| PowerManager.ON_AFTER_RELEASE, TAG);
		mLocClient = ((AppLinkApplication) getApplication()).mLocationClient;
		instance = this;
		// proxy1 = AppLinkService.getProxy();

		try {
			faceView = new FaceView(this);
			mPreview = new CameraPreview(this, faceView);

			FrameLayout layout = (FrameLayout) findViewById(R.id.camera_preview);
			layout.addView(mPreview);
			layout.addView(faceView);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			new AlertDialog.Builder(this).setMessage(e.getMessage()).create()
					.show();
		}

		mPushBtn = (Button) findViewById(R.id.btn_push);
		mPushBtn.setText(pushStatus ? R.string.off : R.string.on);
		mPushBtn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				pushStatus = !pushStatus;
				mPushBtn.setText(pushStatus ? R.string.off : R.string.on);

			}
		});

		Button bindBtn = (Button) findViewById(R.id.btn_bind);
		bindBtn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				PushManager.startWork(getApplicationContext(),
						PushConstants.LOGIN_TYPE_API_KEY,
						Utils.getMetaValue(MainActivity.this, "api_key"));
			}

		});

		Button mSetNumBtn = (Button) findViewById(R.id.btn_setnum);
		mSetNumBtn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				AlertDialog.Builder alert = new AlertDialog.Builder(instance);
				alert.setTitle(R.string.setnum);
				alert.setMessage(R.string.enternum);

				// Set an EditText view to get user input
				final EditText input = new EditText(instance);
				input.setText(AppLinkService.EMERGENTNUMBER);
				alert.setView(input);

				alert.setPositiveButton(R.string.ok,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int whichButton) {
								String value = input.getText().toString();
								Log.d(TAG, "number : " + value);
								AppLinkService.EMERGENTNUMBER = value;

								return;
							}
						});

				alert.setNegativeButton(R.string.cancel,
						new DialogInterface.OnClickListener() {

							public void onClick(DialogInterface dialog,
									int which) {
								// TODO Auto-generated method stub
								return;
							}
						});
				alert.show();

			}
		});

		mStopBtn = (Button) findViewById(R.id.btn_stop);
		mStopBtn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(AppLinkService.PAUSE_MUSIC_ACTION);
				sendBroadcast(intent);
			}
		});

		Button alertBtn = (Button) findViewById(R.id.btn_alert);
		alertBtn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(AppLinkService.EYE_EDTECTED_ACTION);
				sendBroadcast(intent);
			}
		});

		PushConstants.restartPushService(this);
	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		Log.d(TAG, ">=====onStart=====<");
		PushManager.activityStarted(this);
	}

	@Override
	public void onStop() {
		super.onStop();
		Log.d(TAG, ">=====onStop=====<");
		PushManager.activityStoped(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

	protected void onDestroy() {
		Log.v(TAG, "onDestroy main");
		instance = null;
		AppLinkService serviceInstance = AppLinkService.getInstance();
		if (serviceInstance != null) {
			serviceInstance.setCurrentActivity(null);
		}
		mLocClient.stop();
		super.onDestroy();
	}

	protected void onResume() {
		activityOnTop = true;

		super.onResume();

		// Open the default i.e. the first rear facing camera.

		mWakeLock.acquire();
	}

	protected void onPause() {
		activityOnTop = false;
		super.onPause();

		if (mWakeLock != null && mWakeLock.isHeld()) {
			mWakeLock.release();
		}
	}

	public boolean isActivityonTop() {
		return activityOnTop;
	}

	public static Camera getFrontCamera() {
		Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
		for (int id = 0; id < Camera.getNumberOfCameras(); id++) {
			Camera.getCameraInfo(id, cameraInfo);
			if (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
				Camera c = null;
				try {
					c = Camera.open(id);
				} catch (Exception e) {
					Log.d(TAG, "get camera failed.");
					return null;
				}
				return c;
			}
		}
		Log.d(TAG, "can not find front camera.");
		return null;
	}
}
