package com.baidu.fatiguedrive;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class EyeReceiver extends BroadcastReceiver {

	private static final String TAG = "EyeReceiver";
	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO Auto-generated method stub

		Log.d(TAG, "receive eye intent:" + intent.getAction());
		
		if (AppLinkService.EYE_EDTECTED_ACTION.equals(intent.getAction())) {
			Intent eyeIntent = new Intent(context,
                    AppLinkService.class);
            eyeIntent.setAction(AppLinkService.EYE_EDTECTED_ACTION);
            context.startService(eyeIntent);
		} else if (AppLinkService.SEND_SMS_ACTION.equals(intent.getAction())) {
			Intent sendsmsIntent = new Intent(context, AppLinkService.class);
			sendsmsIntent.setAction(AppLinkService.SEND_SMS_ACTION);
			context.startService(sendsmsIntent);
		}
	}

}
